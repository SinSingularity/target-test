Выполнение задания https://github.com/vashkevich/DevOpsTaskJunior

# DevOps junior task

You need to write a script to deploy this application inside a virtual machine running Windows 2012R2. The image should be taken from [here](https://msdn.microsoft.com/en-us/windowsserver2012r2.aspx).

The script should:
* Check the presence of necessary roles and deploy them if necessary.
* Create a separate pool, site.
* Download the application from Git or obtain it in an archive.
* Put everything in a folder, configure correct permissions.
* Ensure that everything works.
* Notify in Slack using the webhook goo.gl/LWjuda that the task has been completed successfully.
* Log properly in case of problems.

You must:
* Fully understand what your script does and why.

Ideal scenario:
* Maintain server state.
  * If IIS is down, the script should bring it back up.
  * If the application in Git is updated, re-deploy it.

ps. Upon successful message delivery to Slack, the server returns the text **ok** and status **200**.
