#Requires -RunAsAdministrator

# For https://github.com/TargetProcess/DevOpsTaskJunior

param (
    [string]$WebContentUrl = "https://github.com/TargetProcess/DevOpsTaskJunior/archive/master.zip",
    [string]$WebTargetDirectory = "C:\inetpub\TargetProcessDevOpsJunior",
    [string]$WebPoolName = "TargetProcessDevOpsTaskJunior",
    [string]$WebSiteName = "TargetProcessDevOpsTaskJunior",
    [string]$WebhookUrl = "https://hooks.slack.com/services/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"
)

Add-Type -AssemblyName System.IO.Compression.FileSystem

$ErrorActionPreference = "Stop"
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

#
# Setup Chocolatey
#

Write-Host "Checking choco..."
if (-Not (Get-Command -Type Application -ErrorAction Ignore -Name choco)) {
    Write-Host "Installing choco..."
    Set-ExecutionPolicy Bypass -Scope Process -Force; Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
    Write-Host "Install choco complete successfully."
} else {
    Write-Host "Choco was found."
}

#
# Check and install net452
#

Write-Host "Checking dotnet..."
$dotNet = choco list --localonly | Where-Object {$_ -like "DotNet4.5.2*"}
if (-Not $dotNet) {
    Write-Host "Installing dotnet4.5.2..."
    choco install dotnet4.5.2 -y
    Write-Host "Install dotnet4.5.2 complete successfully."

    Restart-Computer
} else {
    Write-Host "Found $dotNet"
}

#
# Setup Windows Feature
#

Write-Host "Checking system features..."
#Get-WindowsFeature Web*
$features = Get-WindowsFeature Web-Server, Web-Asp-Net45, Web-Mgmt-Tools, Web-Mgmt-Console, Web-Scripting-Tools
if ($features | Where-Object {$_.InstallState -eq "Removed"}) {
    Write-Warning $features
    Write-Error "System feature is unavailable for install!"
    exit 1
}

if ($features | Where-Object {$_.InstallState -ne "Available"}) {
    Write-Host "All required features are installed."
} else {
    Write-Host "$($features.Length) not installed."
    Write-Host "Installing features..."
    # https://docs.microsoft.com/en-us/iis/install/installing-iis-85/installing-iis-85-on-windows-server-2012-r2
    #Install-WindowsFeature Web-Server -IncludeAllSubFeature
    Install-WindowsFeature Web-Server, Web-Asp-Net45, Web-Mgmt-Tools, Web-Mgmt-Console, Web-Scripting-Tools
    Write-Host "Install features complete successfully."
}

#
# Setup IIS
#

Write-Host "Importing IIS module..."
Import-Module WebAdministration

$webPoolPath = "IIS:\AppPools\$WebPoolName"
if (-Not (Get-Item $webPoolPath)) {
    Write-Host "Creating pool..."
    New-Item $webPoolPath
    Write-Host "Create pool complete successfully."
}

$webDefaultPath = "IIS:\Sites\Default Web Site"
if (Test-Path $webDefaultPath) {
    Write-Host "Removing default site..."
    Remove-Website "Default Web Site"
    Write-Host "Remove default site complete successfully."
}

$webPath = "IIS:\Sites\$WebSiteName"
if (-Not (Test-Path $webPath)) {
    Write-Host "Creating site..."
    New-Item IIS:\Sites\$WebSiteName -Bindings @{protocol="http";bindingInformation="*:80:"} -PhysicalPath $WebTargetDirectory -ApplicationPool $WebPoolName
    Write-Host "Create site complete successfully."
}

#
# Download application
#

Write-Host "Checking target folder..."
if (-Not (Test-Path WebTargetDirectory)) {
    Write-Host "Creating directory '$WebTargetDirectory'..."
    New-Item WebTargetDirectory -ItemType Directory | Out-Null
    if (-Not (Test-Path WebTargetDirectory)) {
        Write-Error "Create directory '$WebTargetDirectory' failed!"
        exit 1
    }
}
Write-Host "Check target folder complete successfully."

Write-Host "Prepare temp file..."
$webContentTemp = "$env:TEMP\WebContent.zip"
$extractTemp = "$env:TEMP\WebContent"
New-Item $webContentTemp -Type File
try {
    Write-Host "Downloading web content..."
    Invoke-WebRequest -Uri $WebContentUrl -OutFile $webContentTemp
    Write-Host "Download web content complete successfully."

    Write-Host "Extraction archive..."
    if (Test-Path $extractTemp) {
        Remove-Item $extractTemp -Recurse
    }
    [System.IO.Compression.ZipFile]::ExtractToDirectory($webContentTemp, $extractTemp)
    Write-Host "Extract archive complete successfully."

    $src = Get-ChildItem $extractTemp | Select-Object -First 1
    if (-Not $src) {
        Write-Error "Archive content is empty?!"
        exit 1
    }

    Write-Host "Stopping website..."
    Stop-Website $WebSiteName

    Write-Host "Clear target directory..."
    $tryCount = 5
    while ($tryCount -gt 0) {
        try {
            Get-ChildItem $WebTargetDirectory | ForEach-Object {
                Remove-Item $_.FullName -Recurse -Force
            }
            break
        } catch {
            Write-Warning $_.Exception.Message
            Start-Sleep -s 1
        } finally {
            $tryCount--
        }
    }
    if ($tryCount -le 0) {
        Write-Error "Clear target directory error."
        exit 1
    }
    Write-Host "Clear target directory complete successfully."

    $srcPath = $src.FullName
    Write-Host "Copying $srcPath to $WebTargetDirectory..."
    Get-ChildItem $srcPath | ForEach-Object {
        Copy-Item $_.FullName -Destination $WebTargetDirectory -Recurse
    }
    Write-Host "Copy complete successfully."

    # Fix error
    $fixFilePath = Join-Path $WebTargetDirectory "Web.config"
    (Get-Content $fixFilePath) | ForEach-Object {
        $_ -Replace '<system.web.>', '<system.web>' -Replace '<\?xml version="1.0" encoding="utf-8"\?>', '<?xml version="1.0"?>'
    } | Out-File $fixFilePath

    Write-Host "Start website"
    Start-Website $WebSiteName
} finally {
    Remove-Item $webContentTemp
    Remove-Item $extractTemp -Recurse
}

#
# Setup permissions
#

Write-Host "Setup target folder permissions..."
$poolCredentialName = "IIS AppPool\$WebPoolName"
$inherit = [system.security.accesscontrol.InheritanceFlags]"ContainerInherit, ObjectInherit"
$propagation = [system.security.accesscontrol.PropagationFlags]"None"

$acl = Get-Acl $WebTargetDirectory
$accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("NetworkService", "ReadAndExecute", $inherit, $propagation, "Allow")
$acl.AddAccessRule($accessRule)
$accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule($poolCredentialName, "ReadAndExecute", $inherit, $propagation, "Allow")
$acl.AddAccessRule($accessRule)
Set-Acl -aclobject $acl $WebTargetDirectory

Write-Host "Setup target folder permissions complete successfully."

#
# Web site test
#

Write-Host "Checking web site response..."
$response = Invoke-WebRequest "http://localhost"
if ($response.StatusCode -ne 200) {
    Write-Warning $response.Content
    Write-Error "Web site test failed!"
    exit 1
}
Write-Host "Check web site response complete successfully."

#
# Report to Slack webhook
#

Write-Host "Post webhook..."
$webhookJson = @{text="@$env:COMPUTERNAME. Deploy successfully."} | ConvertTo-Json -Compress
$response = Invoke-WebRequest $WebhookUrl -Method Post -ContentType "application/json" -Body $webhookJson
if ($response.StatusCode -ne 200 -or $response.Content -ne "ok") {
    Write-Warning $response.Content
    Write-Error "Webhook post failed!"
    exit 1
}
Write-Host "Post webhook complete successfully."

Write-Host "Deploy successfully."
